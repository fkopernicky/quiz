/**
 * Created by fkope on 10.1.2018.
 */


var choises = document.getElementById('choisess'), //zákradné potrebné premenné
    roundNum,
    bet,
    pointscount,
    mainbtn = document.querySelector('.playGame'),
    headingH = document.querySelector('h1'),
    qInfo = document.getElementById('qInfo'),
    qInfo2 = document.getElementById('qInfo2'),
    rand;
var result = document.querySelector('h2');

if (mainbtn.textContent===""){mainbtn.textContent = 'Začať hru!'}

/**
 * časť po kliknutí na tlačidlo
 */

function buttoncntrl (){


    if (mainbtn.textContent === 'Začať hru!' || mainbtn.textContent === 'Nová hra') {

        pointscount = 1; //prvý bod na začiatok hry
        roundNum = 1; //prvé kolo
        bet = 1; //prvý bod rovno vložený ako vklad
        headingH.textContent='';
        usedIndexes = [];
        result.innerHTML = '';
        /////////// zobraziť údaje ////////
        showInfo(bet, roundNum);
        /////////// zobraziť otázku ////////
        showQuestion(generateQuestionNumber(0));//nula lebo je to prvé kolo
        mainbtn.textContent = 'Odpovedať';
    }
    else if (mainbtn.textContent === 'Odpovedať') {

        //////////// overiť odpoved ////////
        var checkAnsw = document.querySelector('input[name="cho"]:checked');
        if (checkAnsw === null) {alert('Vyberte jednu z odpovedí!');}
        else {
            if (checkAnsw.value === sourcedata[mainbtn.value][2]){ //overí či je odpoveď správna
                console.log('Correct');
                headingH.textContent='Správna odpoved!';
                pointscount += Number(bet);

                ///// zobraziť údaje ////
                showInfoAns(pointscount);

                roundNum++; //dalšie kolo
                mainbtn.textContent = 'Pokračovať';
            }
            else{
                console.log('Incorrect');
                pointscount -= bet;

                if (pointscount===0) {
                    headingH.textContent='GAME OVER';
                    mainbtn.textContent = 'Nová hra';
                    var resultEl = document.createElement('p');
                    resultEl.innerHTML = 'Celkové získané skóre: ' + pointscount;
                    result.appendChild(resultEl);
                }
                else {
                    headingH.textContent='Nesprávna odpoved!';
                    roundNum++; //dalšie kolo
                    mainbtn.textContent = 'Pokračovať';
                    showInfoAns(pointscount);
                }
            }
            if (roundNum === 16) {
                console.log("Koniec hry");
                mainbtn.textContent = 'Nová hra';


                var resultEl = document.createElement('p');
                resultEl.innerHTML = 'Celkové získané skóre: ' + pointscount;
                result.appendChild(resultEl);

                while (qInfo2.firstChild) { //odstráni dáta pri generovaní novej
                    qInfo2.removeChild(qInfo2.firstChild);
                }
            }

            while (choises.firstChild) { //odstráni možnosti pri generovaní novej
                choises.removeChild(choises.firstChild);
            }
            while (qInfo.firstChild) { //odstráni dáta pri generovaní novej
                qInfo.removeChild(qInfo.firstChild);
            }
        }
    }
    else if (mainbtn.textContent === 'Pokračovať') {

        if (isFinite(document.getElementById('betvalue').value) && document.getElementById('betvalue').value > 0 && document.getElementById('betvalue').value <= pointscount)
        {
            bet = document.getElementById('betvalue').value;
            while (qInfo2.firstChild) { //odstráni dáta pri generovaní novej
                qInfo2.removeChild(qInfo2.firstChild);
            }

            console.log(Number(bet));
            headingH.textContent = '';

            showInfo(bet, roundNum);
            showQuestion(generateQuestionNumber(calcbetlvl(Number(bet))));

            mainbtn.textContent = 'Odpovedať';
        }
        else alert('Zadajte správnu stávku');
    }
}

function calcbetlvl (bet){ //bet lvl
    
    if (bet <= 50) {return 0;}
    else if (bet <=100) {return 1;}
    else if (bet <= 250) {return 2;}
    else if (bet <= 500) {return 3;}
    else if (bet <= 1000) {return 4;}
    else {return 5;}
}


/**
 * funkcia na generovanie náhodných čísiel otázok s kontorlou nepoužitia rovnakej otázky dva krát
  * @type {Array}
 */

var usedIndexes = [];
var questionController = [0, 0, 0, 0, 0, 0]; //podľa počtu lvl

function generateQuestionNumber (betlvl) { //funkcia na generovanie čísel a overenie čí už nebola použitá otázka

    rand = bets[betlvl][Math.floor(Math.random() * bets[betlvl].length)]; //generovanie náhodného indexu z bets [bet]

    if (usedIndexes.indexOf(rand) === -1) { //ak true otázka nebola použitá >> pokračujeme
        questionController[betlvl] += 1;
        usedIndexes.push(rand);
        return rand; //vrátiť funckiu na zobrazenie otázky
    }
    else {// false >> generujeme nové číslo otázky   //////// dopracovať možnosť poslednného lvl /////////////
        if (questionController[betlvl] === bets[betlvl].length && betlvl != 5) { //ak na danom lvl boli použité všetky otázky lvl + 1
            return generateQuestionNumber(betlvl+1);
        }
        else {
            return generateQuestionNumber(betlvl); ///////// ak bude čas a chuť rozmýšlať vypracovať možnosť že otázky dôjdu (ale bude ich dosť kód by sa nemal dostať do tohto scenára) /////////
        }
    }
}

/**
 * Časť ktorá zoberie číslo otázky, spracuje a zobrazí dáta
 * @type {Array}
 */

function showQuestion (questionNmbr){

    var opinios = []; //pole na odpovede (kôli prehádzaniu)
    opinios.push(sourcedata[questionNmbr][2], sourcedata[questionNmbr][3][0], sourcedata[questionNmbr][3][1]);
    opinios.sort(function(){return 0.5 - Math.random()}); // prehádzanie možností

    var questions = document.createElement('h2');
    questions.textContent = roundNum + ". " + sourcedata[questionNmbr][1];
    choises.appendChild(questions);

    for (var i = 0; i < opinios.length; i++)
    {
        var opinionlabel = document.createElement('label'),
            opinion = document.createElement('input');
        opinion.value = opinios[i];
        opinion.id = opinios[i];
        opinion.type = 'radio';
        opinion.name = 'cho';
        choises.appendChild(opinion);
        opinionlabel.textContent = opinios[i];
        opinionlabel.htmlFor = opinios[i];
        choises.appendChild(opinionlabel);

        var linebrk = document.createElement('br');
        choises.appendChild(linebrk);
    }

    mainbtn.value = questionNmbr;

}

function showInfo (bet, roundNum){

    var qInformation = [];
    qInformation.push("Stávka: " + bet, " | ", "Otázka číslo: " + roundNum +"/15");

    for (var i = 0; i < qInformation.length; i++)
    {
        var qInfoEl = document.createElement('p');
        qInfoEl.innerHTML = qInformation[i];
        qInfo.appendChild(qInfoEl);
    }
}

function showInfoAns (points){
    var h3InfoEL = document.createElement('h3');
    h3InfoEL.innerHTML = 'Tvoja stávka';
    qInfo2.appendChild(h3InfoEL);
    var pInfoEL = document.createElement('p');
    pInfoEL.innerHTML = '(max: ' + points + ' bodov)';
    qInfo2.appendChild(pInfoEL);
    var inputInfoEL = document.createElement('input');
    inputInfoEL.value = points;
    inputInfoEL.id = 'betvalue';
    qInfo2.appendChild(inputInfoEL);
}


